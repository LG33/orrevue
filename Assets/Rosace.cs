﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyAR;

public class Rosace : MonoBehaviour {

	public AudioSource sourcePoem;
	public GameObject subrosace;
	public GameObject buttonText;
	public GameObject text;
	public Button iconClose;
	public GameObject share;
	public GameObject ctnrStop;
	public Button stop;

	private bool textIsShown = false;
	private bool rosaceIsHidden = true;

	void Start () {
		//handler open text
		Button openText = buttonText.GetComponent<Button>();
		openText.onClick.AddListener (OpenText);

		//handler close text
		Button closeText = iconClose.GetComponent<Button>();
		closeText.onClick.AddListener (CloseText);

		//handler stop poem
		Button stopPoem = stop.GetComponent<Button>();
		stopPoem.onClick.AddListener (StopPoem);

		ImageTargetBehaviour.currentTarget.PoemReady (this);

		if (!ImageTargetBehaviour.currentTarget.targetFound) {
			MeshOff ();
		}
	}
		
	void Update()
	{
		if (!sourcePoem.isPlaying) {
			Debug.Log ("poem is not playing");
			StopPoem ();
		}
	}


	// TEXT 
	public void OpenText()
	{
		text.SetActive (true);
		buttonText.SetActive (false);
		share.SetActive (false);
		ctnrStop.SetActive (false);
		textIsShown = true;
	}

	public void CloseText()
	{
		text.SetActive (false);
		textIsShown = false;

		if (rosaceIsHidden) {
			buttonText.SetActive (true);
			share.SetActive (false);
			ctnrStop.SetActive (true);
		} else {
			buttonText.SetActive (true);
			share.SetActive (true);
			ctnrStop.SetActive (true);
			MeshOn ();
		}
	}

	// MESH CONTROL

	void MeshOff()
	{
		rosaceIsHidden = true;
		subrosace.SetActive(false);
	}

	void MeshOn()
	{
		subrosace.SetActive(true);
		rosaceIsHidden = false;
	}


	// PLAYER
	public void OpenPoem()
	{
		if (!textIsShown) {
			if (rosaceIsHidden) {
				Debug.Log ("open poem " + name);
				buttonText.SetActive (true);
				share.SetActive (true);	
				ctnrStop.SetActive (true);
				MeshOn ();
			}

			if (!sourcePoem.isPlaying) 
				sourcePoem.Play ();
		}
	}

	public void StopPoem()
	{
		ImageTargetBehaviour.currentTarget.StopPoem ();
	}

	public void LostPoem()
	{
		Debug.Log("lost poem " + name);
		share.SetActive (false);
		MeshOff ();
	}
}

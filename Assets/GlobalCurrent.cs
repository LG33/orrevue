﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyAR;

public class GlobalCurrent : MonoBehaviour {

	public GameObject introUI, loadingScreen;

    public static GlobalCurrent singleton;
	void Awake()
	{
        singleton = this;
	}

	public void StopLoading()
	{
		if (ImageTargetBehaviour.aRosaceIsPlaying)
			ImageTargetBehaviour.currentTarget.StopPoem ();
	}
}

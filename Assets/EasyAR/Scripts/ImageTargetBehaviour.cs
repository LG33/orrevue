//=============================================================================================================================
//
// Copyright (c) 2015-2017 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
// EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
// and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//=============================================================================================================================

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace EasyAR
{


	public class ImageTargetBehaviour : ImageTargetBaseBehaviour
	{
		public string sceneName;

		private GameObject introUI;
		private GameObject loadingScreen;

		public bool targetFound = false;
		private bool rosaceLoading = false;
		private bool rosaceCreated = false;

		Rosace rosace;

		public static ImageTargetBehaviour currentTarget;
		public static bool aRosaceIsPlaying = false;

		protected override void Awake()
		{
			Path = GetCorrectPath (Path);

			base.Awake();

			TargetFound += OnTargetFound;
			TargetLost += OnTargetLost;

			/*SceneManager.sceneLoaded += OnSceneLoaded;
			SceneManager.sceneUnloaded += OnSceneUnloaded;*/
		}

		public string GetCorrectPath(string filename) {
			if (Application.platform==RuntimePlatform.Android && Application.dataPath.Contains(".obb")) {
				filename = Application.persistentDataPath + "/" + filename;
			} else {
				filename = Application.streamingAssetsPath + "/" + filename;
			}
			Debug.Log (filename);
			return filename;
		}

		protected override void Start()
		{
			base.Start ();

			introUI = GlobalCurrent.singleton.introUI;
			loadingScreen = GlobalCurrent.singleton.loadingScreen;
		}

		protected override void Update()
		{
			base.Start ();

			if (targetFound) {
				if (!introUI.activeSelf) {
					if (!aRosaceIsPlaying && !rosaceCreated && !rosaceLoading) {
						SceneManager.LoadSceneAsync (sceneName, LoadSceneMode.Additive);
						currentTarget = this;
						aRosaceIsPlaying = true;
						rosaceLoading = true;
						loadingScreen.SetActive (true);
						Debug.Log ("start load " + Target.Name);
					} else if (rosaceCreated) {
						rosace.OpenPoem();
					}
				}
			}
		}

		void OnTargetFound(TargetAbstractBehaviour behaviour)
		{
			Debug.Log("Found: " + Target.Name); 
			targetFound = true;
		}

		void OnTargetLost(TargetAbstractBehaviour behaviour)
		{
			Debug.Log("Lost: " + Target.Name);
			targetFound = false;
			if (rosaceLoading) {
				/*SceneManager.UnloadSceneAsync (sceneName);
				currentTarget = null;
				aRosaceIsPlaying = false;
				rosaceLoading = false;
				loadingScreen.SetActive (false);*/
			} else if (rosaceCreated) {
				rosace.LostPoem ();
			}
		}

		public void PoemReady(Rosace rosace) {
			Debug.Log ("poem ready " + Target.Name + " " + rosace.name);
			this.rosace = rosace;
			rosaceLoading = false;
			loadingScreen.SetActive (false);
			rosaceCreated = true;
		}

		public void StopPoem() {
			Debug.Log("StopPoem: " + Target.Name);
			SceneManager.UnloadSceneAsync (sceneName);
			//if (rosaceCreated) {
				aRosaceIsPlaying = false;
				rosaceCreated = false;
				currentTarget = null;
				rosace = null;
				targetFound = false;
			//}
		}

		void OnSceneLoaded(Scene scene, LoadSceneMode mode)
		{
			if (rosaceLoading && scene.name == sceneName) {
				Debug.Log("OnSceneLoaded: " + scene.name);
			}
		}

		void OnSceneUnloaded(Scene scene)
		{
			if (scene.name == sceneName) {
				Debug.Log("OnSceneUnloaded: " + scene.name);
				if (rosaceLoading && aRosaceIsPlaying) {
					rosaceLoading = false;
					loadingScreen.SetActive (false);
					aRosaceIsPlaying = false;
					rosaceCreated = false;
					currentTarget = null;
					rosace = null;
				}
			}
		}
	}
}
